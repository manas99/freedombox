#!/usr/bin/python3

# importing libraries
import argparse
import json
import subprocess

CONF_BASE_DIR = '/etc/mailer/'  # base directory to store all my conf files
CONF_JSON = 'configuration.json'  # To show the conf values on the conf page
CONF_MSMTP = '.msmtprc'  # Actual MSMTP conf file.
PATH_CONF_JSON = CONF_BASE_DIR + '/' + CONF_JSON  # Absolute path to JSON conf
PATH_CONF_MSMTP = CONF_BASE_DIR + '/' + CONF_MSMTP  # Abs path to MSMTP conf

# format for MSMTP conf file.
FORMAT_CONF = """
# Set default values for all following accounts.
defaults
auth on
tls on
tls_trust_file /etc/ssl/certs/ca-certificates.crt
logfile -

account mailer
host {}
port {}
from {}
user {}
password {}

# Set a default account
account default : mailer
"""

# format of the mail that will be sent to MSMTP
FORMAT_MAIL = """To: {}
Subject: {}

{}
"""


def parse_arguments():
    """Return parsed command line arguments as dictionary."""
    parser = argparse.ArgumentParser()
    sub = parser.add_subparsers(dest='subcommand', help='Sub command')

    set_ = sub.add_parser('set-config', help='Set the config values.')
    # Required args: --host, --port, --from_name, --email, --password
    set_.add_argument('--host', type=str, required=True)
    set_.add_argument('--port', type=int, required=True)
    set_.add_argument('--from_name', type=str, required=True)
    set_.add_argument('--email', type=str, required=True)
    set_.add_argument('--password', type=str, required=True)

    sub.add_parser('get-config', help='Return the current config.')

    send_ = sub.add_parser('send-mail', help='Send a mail.')
    # Required args: --to, --subject, --body
    send_.add_argument('--to', type=str, required=True)
    send_.add_argument('--subject', type=str, required=True)
    send_.add_argument('--body', type=str, required=True)

    sub.required = True
    return parser.parse_args()


def subcommand_get_config(args):
    """
    Return the current configuration in JSON format from the JSON conf file.
    """
    configuration = open(PATH_CONF_JSON, 'r').read()
    print(configuration)


def subcommand_set_config(args):
    """Configure MSMTP conf file and store values in JSON conf file."""
    d = {
        'host': args.host,
        'port': args.port,
        'from_name': args.from_name,
        'email': args.email,
        'password': args.password,
    }

    conf = json.dumps(d, indent=4, sort_keys=True)

    open(PATH_CONF_JSON, 'w').write(conf)
    open(PATH_CONF_MSMTP, 'w+').write(FORMAT_CONF.format(
        d['host'], d['port'], d['from_name'], d['email'], d['password']))


def subcommand_send_mail(args):
    """Send a mail by using MSMTP called by subprocess."""
    s = subprocess.Popen(["msmtp", "-C", PATH_CONF_MSMTP,
                          "-t"], stdin=subprocess.PIPE)
    s.communicate(FORMAT_MAIL.format(
        args.to, args.subject, args.body).encode())


def main():
    """Parse arguments and perform all duties."""
    arguments = parse_arguments()
    subcommand = arguments.subcommand.replace('-', '_')
    subcommand_method = globals()['subcommand_' + subcommand]
    subcommand_method(arguments)


if __name__ == '__main__':
    main()
