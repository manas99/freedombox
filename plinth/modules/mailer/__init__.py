"""
Mailer.
This module allows other apps or users to send mails using MSMTP.
It installs the client and also provides configuration options.
"""

from django.utils.translation import ugettext_lazy as _

from plinth import app as app_module
from plinth.modules.users.components import UsersAndGroups
from plinth import actions, menu

from . import manifest


app = None
managed_packages = ['msmtp']  # The packages that nedd to be installed

name = _('Mailer')
version = 1
description = [
    _("Mailing client based on the MSMTP SMTP client."),
    _("Host: Host SMTP server URL<br>"
      "Email: Email ID of the user<br>"
      "Password: User's password"),
    _("<i>Note: For Gmail SMTP server, you need to"
      "generate an app password.</i>")
]
short_description = _('MSMTP Mailer')
groups = {  # Groups which can access the Mailer module from the web interface
    'admin': _('Access to all services and system settings')
}


class MailerApp(app_module.App):
    """FreedomBox app for Mail notifications using MSMTP."""

    app_id = 'mailer'

    def __init__(self):
        """Create components for the app."""
        super().__init__()
        # Info module
        info = app_module.Info(app_id=self.app_id, version=version,
                               name=name,
                               icon='fa-envelope',
                               short_description=short_description,
                               clients=manifest.clients,
                               description=description,)
        self.add(info)
        # Menu module to provide links to access configuration page.
        _menu = menu.Menu('menu-mailer',
                          name=info.name,
                          short_description=info.short_description,
                          icon=info.icon,
                          url_name='mailer:index',
                          parent_url_name='apps')
        self.add(_menu)

        # UsersAndGroups module to set which users can use the app.
        users_and_groups = UsersAndGroups('users-and-groups-mailer',
                                          groups=groups)
        self.add(users_and_groups)


def init():
    """
    Module-scope initialization function called by Plinth.
    Checks if the app needs setup.
    """
    global app
    app = MailerApp()
    # Load the helper from global scope (prepared by Plinth)
    setup_helper = globals()['setup_helper']
    if setup_helper.get_state() != 'needs-setup' and app.is_enabled():
        app.set_enabled(True)


def setup(helper, old_version=None):
    """Installs and configures the MSMTP module on the system."""
    helper.install(managed_packages)


def send_mail(to, subject, body):
    """
    Function that can be called to send mails via other apps.
    Returns True if mail was sent successfully else returns False.
    It calls a command mentioned in the action folder.
    """
    out = actions.superuser_run(
        'mailer.py', ['send-mail', '--to', to,
                      '--subject', subject, '--body', body])
    resp = {}
    key, value = 'data', ''
    for x in out.split(" "):
        if "=" not in x:
            value += " " + x
        else:
            resp[key] = value
            key, value = x.split("=")
    resp[key] = value
    if int(resp['smtpstatus']) <= 299 and int(resp['smtpstatus']) >= 200:
        return True
    return False
