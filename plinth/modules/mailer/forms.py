# from django.utils.translation import ugettext_lazy as _

from django import forms


class MailerConfigForm(forms.Form):
    """Mailer configuration form"""
    host = forms.CharField(max_length=100)
    port = forms.IntegerField(min_value=0)
    from_name = forms.CharField()
    email = forms.EmailField()
    password = forms.CharField(max_length=100, widget=forms.PasswordInput)


class MailerAppForm(forms.Form):
    """Form to send a mail"""
    to = forms.EmailField(max_length=100)
    subject = forms.CharField(max_length=300)
    body = forms.CharField(widget=forms.Textarea)
