from django.utils.translation import ugettext_lazy as _

# Lists the clients who can access the Mailer service
clients = [{
    'name': _('Mailer'),
    'platforms': [{
        'type': 'web',
        'url': '/plinth/apps/mailer/sendmail'
    }]
}]
