"""
URLs for the Mailer module.
"""

from django.conf.urls import url

from .views import MailerAppView, MailerConfigView

urlpatterns = [
    url(r'^apps/mailer/$', MailerConfigView.as_view(),
        name='index'),  # URL leading to a view that configures the Mailer
    url(r'^apps/mailer/sendmail/$', MailerAppView.as_view(),
        name='sendmail'),  # URL leading to a view that sends a mail
]
