import json

from django.contrib import messages
from django.utils.translation import ugettext as _

from plinth import actions, views

from .forms import MailerAppForm, MailerConfigForm
from . import send_mail


class MailerConfigView(views.AppView):
    """Serve configuration page."""
    form_class = MailerConfigForm
    app_id = 'mailer'

    def get_context_data(self, **kwargs):
        """Return additional context for rendering the template."""
        context = super().get_context_data(**kwargs)
        return context

    def get_initial(self):
        """Get the current configuration for MSMTP by calling an action."""
        recvd_conf = actions.superuser_run('mailer.py', ['get-config'])
        recvd_conf = json.loads(recvd_conf)
        return recvd_conf

    def form_valid(self, form):
        """
        Apply the changes to the configuration files as submitted in the form.
        """
        conf = form.cleaned_data
        actions.superuser_run('mailer.py',
                              ['set-config',
                               '--host', conf['host'],
                               '--port', str(conf['port']),
                               '--from_name', conf['from_name'],
                               '--email', conf['email'],
                               '--password', conf['password']])
        messages.success(self.request, _('Configuration updated'))

        return super().form_valid(form)


class MailerAppView(views.AppView):
    """Send mail page."""
    form_class = MailerAppForm
    app_id = 'mailer'
    template_name = 'mailer_sendmail.html'

    def get_context_data(self, **kwargs):
        """Return additional context for rendering the template."""
        context = super().get_context_data(**kwargs)
        return context

    def get_initial(self):
        """Nothing to get."""
        status = super().get_initial()
        return status

    def form_valid(self, form):
        """Send the mail by calling the function from the Mailer module."""
        mail = form.cleaned_data
        if send_mail(mail['to'], mail['subject'], mail['body']):
            messages.success(self.request, _('Mail sent!'))
        else:
            messages.error(self.request, _('An error occured'))
        return super().form_valid(form)
